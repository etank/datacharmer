# DataCharmer homework for Liquid Web

This repository holds the different pieces of work that were dont to attempt to answer the homework problem that was presented.

```raw
##Problem
Using the sample employee dataset found [here](https://github.com/datacharmer/test_db), generate a report showing how much each department is spending on employee salaries each quarter.

##Requirements
Provide the means to generate the report and the prove that it is correct.

datacharmer/test_db
test_db - A sample MySQL database with an integrated test suite, used to test your applications and database servers

This problem is purposely ambiguous…  Setup of the problem is as important
```

## Go Pieces

To use the main.go file included a database server will already need to be ready with the sql data loaded. See the Ansible portion below. You will also need a working go installation including having your $GOPATH set correctly.

The following go packages will need to be present on the system to build / run the code:
* "github.com/tkanos/gonfig"
* "github.com/go-sql-driver/mysql"

To use these run the following commands:

```bash
go get github.com/tkanos/gonfig
go get github.com/go-sql-driver/mysql
```

### Go config file

The app relies on a config file (defaults to ./config.json) to read the database connection information.

#### Example
```json
{
    "DBUser":     "dcharmer_user",
    "DBPass":     "dcharmer_pass",
    "DBName":     "employees",
    "DBHost":     "localhost",
    "DBProtocol": "tcp"
}
```

### Running the app

You can run the app from the main.go file like so:

```bash
go run main.go
```

By default the app will query the database and generates a report for each quarter grouped by year. 

There are 2 flags available:

* -year - Used to limit the report to a specific year. (Default: "")
* -config - Used to specify the path to a config file. (Default: ./config.json)

## Ansible

You will need to have ansible and ansible-galaxy installed on your system before being able to use the included playbook. An ansible.cfg is included for your use.

### Configuration

There are a few variables that will need to be updated before running the playbook.

#### Roles

The playbook makes use of a role from ansible-galaxy named geerlingguy.mysql. This can be installed using the requirements.yml file in the ansible directory:

```bash
ansible-galaxy install -r requirements.yml
```

This will add the role to `$HOME/.ansible/roles/geerlingguy.mysql`

#### Host Vars
There are 3 variables that should be changed in the ansible/host_vars/datacharmer file:
* mysql_root_password:     
* db_user: 
* db_password:

It is a good idea to use a tool like ansible-vault to encrypt the vars used for the root password as well as the db_password that the application will use. Create a file named `.vault-password` in your home directory with the passphrase that you would like to use to encrypt / decrypt data created by ansible-vault.

```bash
ansible-vault encrypt_string SOMETHING_TO_ENCRYPT_HERE
```

This will output an encrypted version of the string that was entered that looks like this:

```raw
!vault |
          $ANSIBLE_VAULT;1.1;AES256
          38626162663637633839626135643766363365363563333463363539613837373463373862613133
          3764393661343635666432323661373837613761666636350a356636306331326464333063396232
          31613162396533626566373262353062663930653132353961336366346431373466323261626166
          3666666338363765340a316564383832663238626234663634656532353732396466393434666237
          31663165653238623633663232313766653632333530353731386339313930666534
```

Use this in the host_var for the variable that it corresponds to.

Example:
```raw
db_password: !vault |
          $ANSIBLE_VAULT;1.1;AES256
          64313066623936313337633361336337303564313563643535343862663334343566313634653261
          6237366262303330636561356566666134626137663162640a616636356130646138353534376364
          64343264613332366535376263373130306263396239633862396234623933343635633565383836
          6633633134346235380a636535336463316334306233326338653934346665343864353336356262
          6332
```

#### Inventory file
In the file ansible/inventory/datacharmer update the IP address with the correct information for your database server.

### Running the playbook

Use the following command to deploy the database.

```bash
ansible-playbook -i inventory/datacharmer deploy_database.yml
```

### Caveats

Unfortunately I was unable to get either the mysql_user module or the geerlingguy.mysql role to correctly add the user with the ability to connect remotely. If that is desired then the user will need to be manually added. You can use something like this to do so:

```bash
[root@datacharmer ~]# mysql
MariaDB [(none)]> GRANT ALL PRIVILEGES on employees.* to 'USERNAME'@'%' IDENTIFIED by 'PASSWORD_HERE';
```