package main

import (
	"database/sql"
	"flag"
	"fmt"

	_ "github.com/go-sql-driver/mysql"
	"github.com/tkanos/gonfig"
)

// Configuration holds the config data parsed from the json file
type Configuration struct {
	DBUser     string
	DBPass     string
	DBName     string
	DBHost     string
	DBProtocol string
}

// SalarySummary holds results from db query
type SalarySummary struct {
	Salary  int
	Quarter int
	Year    string
}

// SalaryHistory accepts a db connection along with a config struct and an optional year string and returns the SalarySummary from the database
func SalaryHistory(db *sql.DB, s SalarySummary, config Configuration, year string) (dataRows *sql.Rows, err error) {
	var queryString string
	switch year {
	case "":
		queryString = fmt.Sprintf("SELECT YEAR(salaries.from_date), QUARTER(salaries.from_date), SUM(salaries.salary) from salaries left join employees on salaries.emp_no = employees.emp_no GROUP BY YEAR(salaries.from_date), QUARTER(salaries.from_date)")
	default:
		queryString = fmt.Sprintf("SELECT YEAR(salaries.from_date), QUARTER(salaries.from_date), SUM(salaries.salary) from salaries left join employees on salaries.emp_no = employees.emp_no WHERE YEAR(salaries.from_date) = %s group by QUARTER(salaries.from_date)", year)
	}
	rows, err := db.Query(queryString)
	if err != nil {
		defer rows.Close()
		return rows, err
	}
	return rows, err
}

func main() {
	var s SalarySummary

	yearPtr := flag.String("year", "", "Specify year to query for salary summaries.")
	configFile := flag.String("config", "config.json", "Specify the path to the config file to parse.")
	flag.Parse()

	config := Configuration{}
	err := gonfig.GetConf(*configFile, &config)
	if err != nil {
		fmt.Println(err)
	}

	db, err := sql.Open("mysql", fmt.Sprintf("%s:%s@%s(%s)/%s", config.DBUser, config.DBPass, config.DBProtocol, config.DBHost, config.DBName))
	defer db.Close()
	if err != nil {
		fmt.Println(err.Error())
	}

	err = db.Ping()
	if err != nil {
		fmt.Println(err.Error())
	}

	rows, err := SalaryHistory(db, s, config, *yearPtr)
	for rows.Next() {
		err := rows.Scan(&s.Year, &s.Quarter, &s.Salary)
		if err != nil {
			fmt.Println(err)
		}
		fmt.Printf("Year: %s\t Quarter: %d\n    Salary Summary: %d\n", s.Year, s.Quarter, s.Salary)
	}
}
